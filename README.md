# mst-simplerpc

[[_TOC_]]

## Installation

Command Line:

```bash
pip install mst-simplerpc --index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
```

requirements.txt

```text
--extra-index-url https://git.mst.edu/api/v4/groups/it%2Fshared%2Fmst-python-libraries/-/packages/pypi/simple
mst-simplerpc
```

## Usage

mst-simplerpc is used to access our simplified RPC implementation

```python
from mst.simplerpc import SimpleRPCClient

rpc_client = SimpleRPCClient(base_url="<host>/perl-bin")

result = rpc_client.<method>()
```
